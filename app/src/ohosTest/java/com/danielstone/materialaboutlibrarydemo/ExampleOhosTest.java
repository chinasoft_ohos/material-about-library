package com.danielstone.materialaboutlibrarydemo;

import com.danielstone.materialaboutlibrarydemo.utils.PxUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.junit.Assert;
import org.junit.Test;


public class ExampleOhosTest extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Test
    public void testFormatNum1() {
        int num = 10;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.0", result);
    }
    @Test
    public void testFormatNum2() {
        float num = 10.543f;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.54", result);
    }

}