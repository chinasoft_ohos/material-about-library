package com.danielstone.materialaboutlibrarydemo;

import ohos.aafwk.ability.fraction.Fraction;

public class FractionInfo {
    private int viewId;
    private Fraction fraction;

    public int getViewId() {
        return viewId;
    }

    public Fraction getFraction() {
        return fraction;
    }

    @Override
    public String toString() {
        return "FractionInfo{" +
                "viewId=" + viewId +
                ", fraction=" + fraction +
                '}';
    }

    private FractionInfo() {
    }

    public static class Builder {
        private int viewId;
        private Fraction fraction;

        public Builder() {
        }

        public Builder setViewId(int viewId) {
            this.viewId = viewId;
            return this;
        }

        public Builder setFraction(Fraction fraction) {
            this.fraction = fraction;
            return this;
        }

        public FractionInfo Build() {
            FractionInfo fractionInfo = new FractionInfo();
            fractionInfo.fraction = this.fraction;
            fractionInfo.viewId = this.viewId;
            return fractionInfo;
        }
    }

}
