package com.danielstone.materialaboutlibrarydemo;

public interface MaterialAboutItemOnClickAction {
    void onClick();
}
