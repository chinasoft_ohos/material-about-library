/*
 * Copyright (c) 2014 MrEngineer13
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.danielstone.materialaboutlibrarydemo.snack;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.utils.Sequenceable;

import java.io.IOException;

public class SnackBar {

    public static final short LONG_SNACK = 5000;

    public static final short MED_SNACK = 3500;

    public static final short SHORT_SNACK = 2000;

    public static final short PERMANENT_SNACK = 0;

    private SnackContainer mSnackContainer;

    //    private View mParentView;
    private Component mParentView;

    private OnMessageClickListener mClickListener;

    private OnVisibilityChangeListener mVisibilityChangeListener;

    public interface OnMessageClickListener {

        //        void onMessageClick(Parcelable token);
        void onMessageClick(Sequenceable token);
    }

    public interface OnVisibilityChangeListener {

        /**
         * Gets called when a message is shown
         *
         * @param stackSize the number of messages left to show
         */
        void onShow(int stackSize);

        /**
         * Gets called when a message is hidden
         *
         * @param stackSize the number of messages left to show
         */
        void onHide(int stackSize);
    }

    public SnackBar(Ability ability,Component component) {
        Context context = ability.getContext();
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        ComponentContainer container = (ComponentContainer) component.getComponentParent();
        scatter.parse(ResourceTable.Layout_sb__snack_container,container,true);
        Component v = scatter.parse(ResourceTable.Layout_sb__snack, container,false);
//        v.setPadding(0,50,0,0);
        init(container, v);
    }

    //    public SnackBar(Context context, View v) {
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        inflater.inflate(R.layout.sb__snack_container, ((ViewGroup) v));
//        View snackLayout = inflater.inflate(R.layout.sb__snack, ((ViewGroup) v), false);
//        init((ViewGroup) v, snackLayout);
//    }
    public SnackBar(Context context, Component v) {
        LayoutScatter scatter = LayoutScatter.getInstance(context);
//        scatter.parse(ResourceTable.Layout_sb__snack_container,(ComponentContainer) v,true);
        Component snackLayout = scatter.parse(ResourceTable.Layout_sb__snack,(ComponentContainer) v, false);
        init((ComponentContainer) v, snackLayout);
    }

    //    private void init(ViewGroup container, View v) {
//        mSnackContainer = (SnackContainer) container.findViewById(R.id.snackContainer);
//        if (mSnackContainer == null) {
//            mSnackContainer = new SnackContainer(container);
//        }
//
//        mParentView = v;
//        TextView snackBtn = (TextView) v.findViewById(R.id.snackButton);
//        snackBtn.setOnClickListener(mButtonListener);
//    }
    private void init(ComponentContainer container, Component v) {
        mSnackContainer = (SnackContainer) container.findComponentById(ResourceTable.Id_snackContainer);
//        mSnackContainer = null;
        if (mSnackContainer == null) {
            mSnackContainer = new SnackContainer(container);
        }

        mParentView = v;
        Text snackBtn = (Text) v.findComponentById(ResourceTable.Id_snackButton);
        snackBtn.setClickedListener(mButtonListener);
    }

    public static class Builder {

        private SnackBar mSnackBar;

        private Context mContext;
        private String mMessage;
        private String mActionMessage;
        private int mActionIcon = 0;
        //        private Parcelable mToken;
        private Sequenceable mToken;
        private short mDuration = MED_SNACK;
        //        private ColorStateList mTextColor;
//        private ColorStateList mBackgroundColor;
        private int mTextColor;
        private int mBackgroundColor;
        private int mHeight;
        private boolean mClear;
        private boolean mAnimateClear;
        //        private Typeface mTypeFace;
        private Font mTypeFace;
        private boolean mIsBottom;

        /**
         * Constructs a new SnackBar
         * @param ability the activity to inflate into
         * @param component the component which contains the snackbar
         */
        public Builder(Ability ability,Component component) {
            mContext = ability.getApplicationContext();
            mSnackBar = new SnackBar(ability,component);
        }

        /**
         * Constructs a new SnackBar
         *
         * @param context the context used to obtain resources
         * @param v       the view to inflate the SnackBar into
         */
//        public Builder(Context context, View v) {
        public Builder(Context context, Component v) {
            mContext = context;
            mSnackBar = new SnackBar(context, v);
        }

        /**
         * Sets the message to display on the SnackBar
         *
         * @param message the literal string to display
         * @return this builder
         */
        public Builder withMessage(String message) {
            mMessage = message;
            return this;
        }

        /**
         * Sets the message to display on the SnackBar
         *
         * @param messageId the resource id of the string to display
         * @return this builder
         * @throws Exception
         */
        public Builder withMessageId(int messageId) throws Exception {
//            mMessage = mContext.getString(messageId);
            mMessage = mContext.getResourceManager().getSolidXml(messageId).getRoot().getStringValue();
            return this;
        }

        /**
         * Sets the message to display as the action message
         *
         * @param actionMessage the literal string to display
         * @return this builder
         */
        public Builder withActionMessage(String actionMessage) {
            mActionMessage = actionMessage;
            return this;
        }

        /**
         * Sets the message to display as the action message
         *
         * @param actionMessageResId the resource id of the string to display
         * @return this builder
         * @throws Exception
         */
        public Builder withActionMessageId(int actionMessageResId) throws Exception {
            if (actionMessageResId > 0) {
//                mActionMessage = mContext.getString(actionMessageResId);
                mActionMessage = mContext.getResourceManager().getSolidXml(actionMessageResId).getRoot().getStringValue();
            }

            return this;
        }

        /**
         * Sets the action icon
         *
         * @param id the resource id of the icon to display
         * @return this builder
         */
        public Builder withActionIconId(int id) {
            mActionIcon = id;
            return this;
        }

        /**
         * Sets the {@link SnackBar.Style} for the action message
         *
         * @param color the {@link SnackBar.Style} to use
         * @return this builder
         */
//        public Builder withStyle(Style style) {
        public Builder withStyle(int color) {
//            Color color = getActionTextColor(style);
            mTextColor = color;
            return this;
        }

        /**
         * The token used to restore the SnackBar state
         *
         * @param token the parcelable containing the saved SnackBar
         * @return this builder
         */
//        public Builder withToken(Parcelable token) {
//            mToken = token;
//            return this;
//        }
        public Builder withToken(Sequenceable token) {
            mToken = token;
            return this;
        }

        /**
         * Sets the duration to show the message
         *
         * @param duration the number of milliseconds to show the message
         * @return this builder
         */
        public Builder withDuration(Short duration) {
            mDuration = duration;
            return this;
        }

        public Builder withTextColorId(int colorId) throws NotExistException, WrongTypeException, IOException {
//            ColorStateList color = mContext.getResourceManager().getResources().getColorStateList(colorId);
//            Color color = mContext.getResourceManager().getResources().getColorStateList(colorId);
//            int x = mContext.getResourceManager().getElement(colorId).getColor();
//            Color color = new Color(x);
            mTextColor = colorId;
//            Color color =
            return this;
        }

        public Builder withBackgroundColorId(int colorId)  {
//            ColorStateList color = mContext.getResources().getColorStateList(colorId);
//            Color color = mContext.getResources().getColorStateList(colorId);
            //这里如果不行的话，直接用switch获取
//            int x = mContext.getResourceManager().getElement(colorId).getColor();
//            Color color = new Color(x);
            mBackgroundColor = colorId;
            return this;
        }

        /**
         * Sets the height for SnackBar
         *
         * @param height the height of SnackBar
         * @return this builder
         */
        public Builder withSnackBarHeight(int height) {
            mHeight = height;
            return this;
        }

        /**
         * Sets the OnClickListener for the action button
         *
         * @param onClickListener the listener to inform of click events
         * @return this builder
         */
        public Builder withOnClickListener(OnMessageClickListener onClickListener) {
            mSnackBar.setOnClickListener(onClickListener);
            return this;
        }

        /**
         * Sets the visibilityChangeListener for the SnackBar
         *
         * @param visibilityChangeListener the listener to inform of visibility changes
         * @return this builder
         */
        public Builder withVisibilityChangeListener(OnVisibilityChangeListener visibilityChangeListener) {
            mSnackBar.setOnVisibilityChangeListener(visibilityChangeListener);
            return this;
        }

        /**
         * Clears all of the queued SnackBars, animates the message being hidden
         *
         * @return this builder
         */
        public Builder withClearQueued() {
            return withClearQueued(true);
        }

        /**
         * Clears all of the queued SnackBars
         *
         * @param animate whether or not to animate the messages being hidden
         * @return this builder
         */
        public Builder withClearQueued(boolean animate) {
            mAnimateClear = animate;
            mClear = true;
            return this;
        }

        /**
         * Sets the Typeface for the SnackBar
         *
         * @param typeFace the typeface to apply to the SnackBar
         * @return this builder
         */
//        public Builder withTypeFace(Typeface typeFace) {
        public Builder withTypeFace(Font typeFace) {
            mTypeFace = typeFace;
            return this;
        }

        public Builder withAlignBottom(boolean isBottom) {
            mIsBottom = isBottom;
            return this;
        }

        /**
         * Shows the first message in the SnackBar
         *
         * @return the SnackBar
         */
        public SnackBar show() {
//            Snack message = new Snack(mMessage,
//                    (mActionMessage != null ? mActionMessage.toUpperCase() : null),
//                    mActionIcon,
//                    mToken,
//                    mDuration,
//                    mTextColor != null ? mTextColor : getActionTextColor(Style.DEFAULT),
//                    mBackgroundColor != null ? mBackgroundColor : mContext.getResources().getColorStateList(R.color.sb__snack_bkgnd),
//                    mHeight != 0 ? mHeight : 0,
//                    mTypeFace);
            Snack message = new Snack(mMessage,
                    (mActionMessage != null ? mActionMessage.toUpperCase() : null),
                    mActionIcon,
                    mToken,
                    mDuration,
                    mTextColor != 100 ? mTextColor : Color.WHITE.getValue(),
                    mBackgroundColor != 100 ? mBackgroundColor : Color.WHITE.getValue(),
                    mHeight != 0 ? mHeight : 0,
                    mTypeFace, mIsBottom);

            if (mClear) {
                mSnackBar.clear(mAnimateClear);
            }

            mSnackBar.showMessage(message);

            return mSnackBar;
        }

        //        private ColorStateList getActionTextColor(Style style) {
//            switch (style) {
//                case ALERT:
//                    return mContext.getResources().getColorStateList(R.color.sb__button_text_color_red);
//                case INFO:
//                    return mContext.getResources().getColorStateList(R.color.sb__button_text_color_yellow);
//                case CONFIRM:
//                    return mContext.getResources().getColorStateList(R.color.sb__button_text_color_green);
//                case DEFAULT:
//                    return mContext.getResources().getColorStateList(R.color.sb__default_button_text_color);
//                default:
//                    return mContext.getResources().getColorStateList(R.color.sb__default_button_text_color);
//            }
//        }
        private Color getActionTextColor(Style style) {
            switch (style) {
                case ALERT:
                    return Color.RED;
                case INFO:
                    return Color.YELLOW;
                case CONFIRM:
                    return Color.GREEN;
                case DEFAULT:
                    return Color.GRAY;
                default:
                    return Color.GRAY;
            }
        }
    }

    private void showMessage(Snack message) {
        mSnackContainer.showSnack(message, mParentView, mVisibilityChangeListener);
    }

    /**
     * Calculates the height of the SnackBar
     *
     * @return the height of the SnackBar
     */
//    public int getHeight() {
//        mParentView.measure(View.MeasureSpec.makeMeasureSpec(mParentView.getWidth(), View.MeasureSpec.EXACTLY),
//                View.MeasureSpec.makeMeasureSpec(mParentView.getHeight(), View.MeasureSpec.AT_MOST));
//        return mParentView.getMeasuredHeight();
//    }
//    public int getHeight() {
//        mParentView.
//    }

    /**
     * Getter for the SnackBars parent view
     *
     * @return the parent view
     */
//    public View getContainerView() {
//        return mParentView;
//    }
    public Component getContainerView() {
        return mParentView;
    }

    //    private final View.OnClickListener mButtonListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            if (mClickListener != null && mSnackContainer.isShowing()) {
//                mClickListener.onMessageClick(mSnackContainer.peek().mToken);
//            }
//            mSnackContainer.hide();
//        }
//    };
    private final Component.ClickedListener mButtonListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            if (mClickListener != null && mSnackContainer.isShowing()) {
                mClickListener.onMessageClick(mSnackContainer.peek().mToken);
            }
            mSnackContainer.hide();
        }
    };

    private SnackBar setOnClickListener(OnMessageClickListener listener) {
        mClickListener = listener;
        return this;
    }

    private SnackBar setOnVisibilityChangeListener(OnVisibilityChangeListener listener) {
        mVisibilityChangeListener = listener;
        return this;
    }

    /**
     * Clears all of the queued messages
     *
     * @param animate whether or not to animate the messages being hidden
     */
    public void clear(boolean animate) {
        mSnackContainer.clearSnacks(animate);
    }

    /**
     * Clears all of the queued messages
     */
    public void clear() {
        clear(true);
    }

    /**
     * Hides all snacks
     */
    public void hide() {
        mSnackContainer.hide();
        clear();
    }


    /**
     * All snacks will be restored using the view from this Snackbar
     *
     * @param state save snacks to Intent
     */
    public void onRestoreInstanceState(Intent state) {
        mSnackContainer.restoreState(state, mParentView);
    }

    //    public Bundle onSaveInstanceState() {
//        return mSnackContainer.saveState();
//    }
    public Intent onSaveInstanceState() {
        return mSnackContainer.saveState();
    }

    public enum Style {
        DEFAULT,
        ALERT,
        CONFIRM,
        INFO
    }
}
