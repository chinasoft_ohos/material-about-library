package com.danielstone.materialaboutlibrarydemo.fraction;

import com.danielstone.materialaboutlibrarydemo.ConvenienceBuilder;
import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.adapters.MaterialAboutListAdapter;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrarydemo.model.MaterialAboutList;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class LicenseFraction extends BaseFraction {

    private ListContainer recyclerView;
    private MaterialAboutListAdapter adapter;
    private Component root;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        root = super.onComponentAttached(scatter, container, intent);
        return root;
    }

    @Override
    protected void onStart(Intent intent) {
        intent.setParam("name", "License");
        super.onStart(intent);
        Component child = ((ComponentContainer) root).getComponentAt(1);
        recyclerView = (ListContainer) child.findComponentById(ResourceTable.Id_mal_recyclerview);
        initViews();
    }

    @Override
    public int getChildUIContent() {
        return ResourceTable.Layout_mal_material_about_content;
    }

    private void initViews() {
        MaterialAboutCard license1 = ConvenienceBuilder.createLicenseCard(
                getFractionAbility(),
                ResourceTable.Media_icon,
                "material-about-library", "2016", "Daniel Stone",
                ResourceTable.String_license_apache2);

        MaterialAboutCard license2 = ConvenienceBuilder.createLicenseCard(
                getFractionAbility(),
                ResourceTable.Media_icon,
                "Phone Iconics", "2016", "Mike Penz",
                ResourceTable.String_license_apache2);

        MaterialAboutCard license3 = ConvenienceBuilder.createLicenseCard(
                getFractionAbility(),
                ResourceTable.Media_icon,
                "LeakCanary", "2015", "Square, Inc",
                ResourceTable.String_license_apache2);

        MaterialAboutCard license4 = ConvenienceBuilder.createLicenseCard(
                getFractionAbility(),
                ResourceTable.Media_icon,
                "MIT Example", "2017", "Matthew Ian Thomson",
                ResourceTable.String_license_mit);

        MaterialAboutCard license5 = ConvenienceBuilder.createLicenseCard(
                getFractionAbility(),
                ResourceTable.Media_icon,
                "GPL Example", "2017", "George Perry Lindsay",
                ResourceTable.String_license_gpl);

        MaterialAboutList list = new MaterialAboutList(license1, license2, license3, license4, license5);
        adapter = new MaterialAboutListAdapter(list.getCards());
//        recyclerView.setLayoutManager(new DirectionalLayoutManager());
        recyclerView.setItemProvider(adapter);
    }

}
