package com.danielstone.materialaboutlibrarydemo.utils;

import com.danielstone.materialaboutlibrarydemo.FractionInfo;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Stack;

public class FractionHelper {

    private static HashMap<FractionAbility, FractionHelper> fractionHelperHashMap = new HashMap<>();
    FractionAbility fractionAbility;
    FractionInfo nowShow;//当前helper中显示的fraction
    private HashMap<Class, FractionInfo> fractionMap = new HashMap();
    private HashMap<FractionAbility, Stack<Fraction>> fractionStack = new HashMap<>();

    private FractionHelper(FractionAbility fractionAbility) {
        this.fractionAbility = fractionAbility;
        fractionStack = new HashMap<>();
        fractionStack.put(fractionAbility, new Stack<>());
    }

    public static FractionHelper getInstance(FractionAbility fractionAbility) {
        FractionHelper fractionHelper = fractionHelperHashMap.get(fractionAbility);
        if (fractionHelper == null) {
            synchronized (FractionHelper.class) {
                fractionHelper = new FractionHelper(fractionAbility);
                fractionHelperHashMap.put(fractionAbility, fractionHelper);
            }
        }
        return fractionHelper;
    }

    private FractionInfo getFractionInstance(Class<? extends Fraction> fractionClass) {
        try {
            FractionInfo fractionInfo = fractionMap.get(fractionClass);
            if (fractionInfo == null || fractionInfo.getViewId() == 0) {
                throw new NullPointerException("please use getFractionInstance(int compentId, Class<? extends Fraction> fractionClass) method to init");
            } else {
                if (fractionInfo.getFraction() == null) {
                    fractionInfo = new FractionInfo.Builder().setViewId(fractionInfo.getViewId()).setFraction(fractionClass.newInstance()).Build();
                    fractionMap.put(fractionClass, fractionInfo);
                }
                return fractionInfo;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Fraction addFraction(int compentId, Class<? extends Fraction> fractionClass) {
        try {
            FractionInfo fractionInfo = fractionMap.get(fractionClass);
            if (fractionInfo == null || fractionInfo.getFraction() == null) {
                fractionInfo = new FractionInfo.Builder().setViewId(compentId).setFraction(fractionClass.newInstance()).Build();
                fractionMap.put(fractionClass, fractionInfo);
            }
            return fractionInfo.getFraction();
        } catch (Exception e) {
        }
        return null;
    }

    public void showFraction(Class<? extends Fraction> fractionClass) {
        if (fractionMap.get(fractionClass) == null) {
            throw new NullPointerException("please use showFraction(int compentId, Class<Fraction> fractionClass) to bind parent view");
        }
        if (nowShow != null) {
            fractionAbility.getFractionManager().startFractionScheduler().hide(nowShow.getFraction()).submit();
            fractionAbility.getFractionManager().startFractionScheduler().remove(nowShow.getFraction()).submit();
        }
        FractionInfo fractionInfo = getFractionInstance(fractionClass);
        fractionAbility.getFractionManager().startFractionScheduler().add(fractionInfo.getViewId(), fractionInfo.getFraction()).submit();
        fractionAbility.getFractionManager().startFractionScheduler().show(fractionInfo.getFraction()).submit();
        manageFractionStack(fractionInfo.getFraction());
        nowShow = getFractionInstance(fractionClass);
    }

    private void manageFractionStack(Fraction fraction) {
        if (fractionAbility != null) {
            int i = fractionStack.get(fractionAbility).search(fraction);
            if (i == -1) {
                fractionStack.get(fractionAbility).push(fraction);
            } else {
                fractionStack.get(fractionAbility).pop();
                manageFractionStack(fraction);
            }
        }
    }

    public Fraction gotoLastFraction() {
        Fraction fraction;
        if (fractionAbility != null) {
            try {
                fractionStack.get(fractionAbility).pop();//弹出当前Fraction
                fraction = fractionStack.get(fractionAbility).pop();//获取前一个fraction
                showFraction(fraction.getClass());
                return fraction;
            } catch (EmptyStackException e) {
                return null;
            }
        } else {
            return null;
        }

    }

}
