package com.danielstone.materialaboutlibrarydemo.utils;

import com.danielstone.materialaboutlibrarydemo.ResourceTable;
import com.danielstone.materialaboutlibrarydemo.items.ActionItem;
import com.danielstone.materialaboutlibrarydemo.items.MaterialAboutItem;
import com.danielstone.materialaboutlibrarydemo.items.MyCustomItem;
import com.danielstone.materialaboutlibrarydemo.items.TitleItem;
import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import ohos.agp.components.Component;

public class MyViewTypeManager extends ViewTypeManager {

//    public static final class ItemType {
//        public static final int ACTION_ITEM = ViewTypeManager.ItemType.ACTION_ITEM;
//        public static final int TITLE_ITEM = ViewTypeManager.ItemType.TITLE_ITEM;
//        public static final int CUSTOM_ITEM = 10;
//    }
//
//    public static final class ItemLayout {
//        public static final int ACTION_LAYOUT = ViewTypeManager.ItemLayout.ACTION_LAYOUT;
//        public static final int TITLE_LAYOUT = ViewTypeManager.ItemLayout.TITLE_LAYOUT;
//        public static final int CUSTOM_LAYOUT = ResourceTable.Layout_custom_item;
//    }

    public static final int CUSTOM_ITEM = 10;
    public static final int CUSTOM_LAYOUT = ResourceTable.Layout_custom_item;

    public int getLayout(int itemType) {
        switch (itemType) {
            case ItemType.ACTION_ITEM:
                return ItemLayout.ACTION_LAYOUT;
            case ItemType.TITLE_ITEM:
                return ItemLayout.TITLE_LAYOUT;
            case CUSTOM_ITEM:
                return CUSTOM_LAYOUT;
            default:
                return -1;
        }
    }

    public ItemViewHolder getViewHolder(int itemType, Component view) {
        switch (itemType) {
            case ItemType.ACTION_ITEM:
                return ActionItem.getViewHolder(view);
            case ItemType.TITLE_ITEM:
                return TitleItem.getViewHolder(view);
            case CUSTOM_ITEM:
                return MyCustomItem.getViewHolder(view);
            default:
                return null;
        }
    }

    public void setupItem(int itemType, ItemViewHolder holder, MaterialAboutItem item, Component parent) {
        switch (itemType) {
            case ItemType.ACTION_ITEM:
                ActionItem.setupItem((ActionItem.ActionItemViewHolder) holder, (ActionItem) item, parent);
                break;
            case ItemType.TITLE_ITEM:
                TitleItem.setupItem((TitleItem.TitleItemViewHolder) holder, (TitleItem) item, parent);
                break;
            case CUSTOM_ITEM:
                MyCustomItem.setupItem((MyCustomItem.MyCustomItemViewHolder) holder, (MyCustomItem) item, parent);
                break;
        }
    }
}
