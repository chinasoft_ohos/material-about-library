package com.danielstone.materialaboutlibrarydemo;

public enum IconAlignment {
    ALIGNMENT_TOP, ALIGNMENT_MIDDLE, ALIGNMENT_BOTTOM;

    public int getValue() {
        if (this == ALIGNMENT_TOP) {
            return 0;
        } else if (this == ALIGNMENT_MIDDLE) {
            return 1;
        } else if (this == ALIGNMENT_BOTTOM) {
            return 2;
        } else {
            return 1;
        }
    }
}
