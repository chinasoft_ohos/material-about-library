package com.danielstone.materialaboutlibrarydemo.adapters;

import com.danielstone.materialaboutlibrarydemo.utils.DefaultViewTypeManager;
import com.danielstone.materialaboutlibrarydemo.holders.ItemViewHolder;
import com.danielstone.materialaboutlibrarydemo.items.MaterialAboutItem;
import com.danielstone.materialaboutlibrarydemo.utils.ViewTypeManager;
import ohos.agp.components.*;

import java.util.List;

public class MaterialAboutItemAdapter extends BaseItemProvider {

    private List<MaterialAboutItem> data;

    private ViewTypeManager viewTypeManager;
    private LayoutScatter instance;

    MaterialAboutItemAdapter(List<MaterialAboutItem> data) {
        this.data = data;
        this.viewTypeManager = new DefaultViewTypeManager();
    }

    MaterialAboutItemAdapter(ViewTypeManager customViewTypeManager, List<MaterialAboutItem> data) {
        this.data = data;
        this.viewTypeManager = customViewTypeManager;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer container) {
        int type = getItemComponentType(i);
        int layoutId = viewTypeManager.getLayout(type);
        if (instance == null) {
            instance = LayoutScatter.getInstance(container.getContext());
        }
        Component itemView = instance.parse(layoutId, container, false);
        ItemViewHolder holder = viewTypeManager.getViewHolder(type, itemView);
        viewTypeManager.setupItem(type, holder, data.get(i), itemView);
        return itemView;
    }

    @Override
    public int getItemComponentType(int position) {
        return data.get(position).getType();
    }
}
